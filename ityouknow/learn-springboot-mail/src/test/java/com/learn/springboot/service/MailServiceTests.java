package com.learn.springboot.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.io.File;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MailServiceTests {

    @Autowired
    private MailService mailService;

    @Autowired
    private TemplateEngine templateEngine;

    @Test
    public void testSendSimpleMail() {
        mailService.sendSimpleMail("xxx@163.com", "Test Send Simple Mail", "Spring Boot For Send Simple Mail");
    }

    @Test
    public void testSendHtmlMail() {
        String html = "<html>\n" +
                            "<body>\n" +
                                "<h3>Spring Boot For Send Html Mail</h3>\n" +
                            "</body>\n" +
                      "</html>";
        mailService.sendHtmlMail("xxx@163.com", "Test Send Html Mail", html);
    }

    @Test
    public void testSendAttachmentsMail() {
        String filePath = "D:\\images\\xxx.png";
        mailService.sendAttachmentsMail("xxx@163.com","Test Send Attachments Mail","Spring Boot For Send Attachments Mail",filePath);
    }

    @Test
    public void testSendInlineResourceMail() {
        String resId = "20180114";
        String content="<html><body>这是有图片的邮件：<img src=\'cid:" + resId + "\' ></body></html>";
        String filePath = "D:\\images\\xxx.png";

        mailService.sendInlineResourceMail("xxx@163.com", "Test Send InlineResource Mail", content, filePath, resId);
    }

    @Test
    public void sendTemplateMail() {
        //创建邮件正文
        Context context = new Context();
        context.setVariable("id", "20180114");
        String content = templateEngine.process("emailTemplate", context);

        mailService.sendHtmlMail("xxx@163.com","Test Send Template Mail",content);
    }
}
