package com.learn.springboot.service.impl;

import com.learn.springboot.domain.Student;
import com.learn.springboot.repository.StudentRepository;
import com.learn.springboot.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public List<Student> getStudentList() {
        return studentRepository.findAll();
    }

    @Override
    public Student findStudentById(Long id) {
        return studentRepository.findById(id);
    }

    @Override
    public void save(Student user) {
        studentRepository.save(user);
    }

    @Override
    public void edit(Student user) {
        studentRepository.save(user);
    }

    @Override
    public void delete(Long id) {
        studentRepository.delete(id);
    }
}
