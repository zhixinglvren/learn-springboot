package com.learn.springboot.controller;

import com.learn.springboot.domain.Student;
import com.learn.springboot.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StudentController {

    @Autowired
    private StudentService studentService;

    @RequestMapping("/getStudents")
    public List<Student> getStudents() {
        return studentService.getStudentList();
    }

    @RequestMapping("/getStudent/{id}")
    public Student getStudent(@PathVariable("id") Long id) {
        return studentService.findStudentById(id);
    }

    @RequestMapping("/findStudentById")
    public Student findStudentById(Long id) {
        return studentService.findStudentById(id);
    }
}
