package com.learn.springboot.repository;

import com.learn.springboot.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student,Long> {

    Student findById(Long id);

    Long deleteById(Long id);
}
