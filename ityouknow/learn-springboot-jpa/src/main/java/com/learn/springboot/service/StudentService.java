package com.learn.springboot.service;

import com.learn.springboot.domain.Student;

import java.util.List;

public interface StudentService {

    public List<Student> getStudentList();

    public Student findStudentById(Long id);

    public void save(Student user);

    public void edit(Student user);

    public void delete(Long id);
}
