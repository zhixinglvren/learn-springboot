package com.learn.springboot;

import com.learn.springboot.domain.Student;
import com.learn.springboot.service.StudentService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StudentServiceTests {

    @Autowired
    private StudentService studentService;

    @Test
    public void testSave() {
        studentService.save(new Student("20170001","小明","xiaoming@163.com"));
        studentService.save(new Student("20170002","小红","xiaohong@163.com"));
        studentService.save(new Student("20170003","小李","xiaoli@163.com"));
        Assert.assertEquals(3,studentService.getStudentList().size());
    }

    @Test
    public void testFindStudentById() {
        Student student = studentService.findStudentById(1L);
        Assert.assertEquals("小明",student.getUserName());
    }
}
