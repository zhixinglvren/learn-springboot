package com.learn.springboot.scheduler;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by 知行旅人 on 2018/1/10 0010.
 */
@Component
public class SchedulerTaskOne {
    private int count = 0;

    @Scheduled(cron="*/6 * * * * ?")
    private void process() {
        System.out.println("this is scheduler task running " + (count++));
    }
}
