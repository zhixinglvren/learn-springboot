### Spring Boot 项目测试、打包、部署

一. 开发阶段

* 单元测试

1. 在pom包中添加spring-boot-starter-test包引用
```
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-test</artifactId>
	<scope>test</scope>
</dependency>
```

2. 编写测试类，在类上添加`@RunWith(SpringRunner.class)`和`@SpringBootTest`注解，在方法上添加`@Test`注解

Spring Boot还支持Controller层的测试，需要引入`MockMvc`


* 集成测试

运行Aplication启动类，进行调试

Spring Boot支持热部署，需要在`pom.xml`中添加
```
 <dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-devtools</artifactId>
        <optional>true</optional>
    </dependency>
</dependencies>

<build>
    <plugins>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
            <configuration>
                <fork>true</fork>
            </configuration>
        </plugin>
    </plugins>
</build>
```

二. 上线阶段

* Jar包形式

cd 项目根目录（与pom.xml统计） : mvn clean package

如使用IDE工具，可直接在IDE中执行上述命令

排除测试代码后进行打包 : mvn clean package -Dmaven.test.skip=true

打包完成后jar包会生成到target目录下，命名一般是 项目名+版本号.jar

启动jar包命令`java -jar xxx-version.jar`

以后台服务形式启动命令`java -jar xxx-version.jar &`

启动时读取不同环境配置`java -jar xxx-version.jar --spring.profiles.active=dev`

启动时设置jvm参数`java -Xms10m -Xmx80m -jar xxx-version.jar &`


* War包形式

1. pom.xml中使用`<packaging>war</packaging>`

2. pom.xml中配置
```
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-tomcat</artifactId>
    <scope>provided</scope>
</dependency>
```
scope设置为provided是为了打包时排除tomcat,因为war部署在容器中后，容器本身会提供所需的API。

3. 注册启动类

创建ServletInitializer.java，继承SpringBootServletInitializer ，覆盖configure()，把启动类Application注册进去。外部web应用服务器构建Web Application Context的时候，会把启动类添加进去。

```
public class ServletInitializer extends SpringBootServletInitializer {
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(PackageApplication.class);
	}
}
```

后续打包命令和jar包形式基本类似


三. 生产运维

查看JVM参数的值`jinfo -flags pid`

`-XX:CICompilerCount=3 -XX:InitialHeapSize=234881024 -XX:MaxHeapSize=3743416320 -XX:MaxNewSize=1247805440 -XX:MinHeapDeltaBytes=524288 -XX:NewSize=78118912 -XX:OldSize=156762112 -XX:+UseCompressedClassPointers -XX:+UseCompressedOops -XX:+UseFastUnorderedTimeStamps -XX:+UseParallelGC`

-XX:CICompilerCount ：最大的并行编译数

-XX:InitialHeapSize 和 -XX:MaxHeapSize ：指定JVM的初始和最大堆内存大小

-XX:MaxNewSize ： JVM堆区域新生代内存的最大可分配大小

…

-XX:+UseParallelGC ：垃圾回收使用Parallel收集器



