### Web开发

#### JSON接口开发
在以前的spring 开发的时候需要我们提供json接口的时候需要做那些配置呢
* 添加 jackjson 等相关jar包
* 配置spring controller扫描
* 对接的方法添加@ResponseBody

而如果使用Spring Boot，只需要在Controller类添加@RestController注解即可

#### 自定义Filter
我们常常在项目中会使用filters用于录调用日志、排除有XSS威胁的字符、执行权限验证等等。
Spring Boot自动添加了OrderedCharacterEncodingFilter和HiddenHttpMethodFilter，并且我们可以自定义Filter。

两个步骤：

1. 实现Filter接口，实现Filter方法
2. 添加@Configuration 注解，将自定义Filter加入过滤链

#### 自定义Property
配置在application.properties中

#### Log配置
配置输出的地址和输出级别
```
logging.path=/user/local/log
logging.level.com.learn.springboot=DEBUG
logging.level.org.springframework.web=INFO
logging.level.org.hibernate=ERROR
```
path为本机的log地址，logging.level 后面可以根据包路径配置不同资源的log级别

#### 数据库操作
在Spring Boot中如何操作数据库？(以MySQL为例)

1. 添加相关jar包
```
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-jpa</artifactId>
</dependency>
 <dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
</dependency>
```

2. 添加配置文件
```
spring.datasource.url=jdbc:mysql://localhost:3306/learn_springboot
spring.datasource.username=root
spring.datasource.password=123456
spring.datasource.driver-class-name=com.mysql.jdbc.Driver

spring.jpa.properties.hibernate.hbm2ddl.auto=update
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.MySQL5InnoDBDialect
spring.jpa.show-sql=true
```

hibernate.hbm2ddl.auto参数的作用主要用于：自动创建|更新|验证数据库表结构,有四个值：
* create： 每次加载hibernate时都会删除上一次的生成的表，然后根据你的model类再重新来生成新表，哪怕两次没有任何改变也要这样执行，这就是导致数据库表数据丢失的一个重要原因。
* create-drop ：每次加载hibernate时根据model类生成表，但是sessionFactory一关闭,表就自动删除。
* update：最常用的属性，第一次加载hibernate时根据model类会自动建立起表的结构（前提是先建立好数据库），以后加载hibernate时根据 model类自动更新表结构，即使表结构改变了但表中的行仍然存在不会删除以前的行。要注意的是当部署到服务器后，表结构是不会被马上建立起来的，是要等 应用第一次运行起来后才会。
* validate ：每次加载hibernate时，验证创建数据库表结构，只会和数据库中的表进行比较，不会创建新表，但是会插入新值。

dialect 主要是指定生成表名的存储引擎为InneoDB

show-sql 是否打印出自动生产的SQL，方便调试的时候查看

3. 添加实体类和Dao

4. 测试

#### Thymeleaf模板
* Thymeleaf介绍

Thymeleaf是一款用于渲染XML/XHTML/HTML5内容的模板引擎。类似JSP，Velocity，FreeMaker等，它也可以轻易的与Spring MVC等Web框架进行集成作为Web应用的模板引擎。与其它模板引擎相比，Thymeleaf最大的特点是能够直接在浏览器中打开并正确显示模板页面，而不需要启动整个Web应用。

** 注意，由于Thymeleaf使用了XML DOM解析器，因此它并不适合于处理大规模的XML文件。**

* Thymeleaf语法简介

1. URL

Thymeleaf对于URL的处理是通过语法@{…}来处理的
```
<a th:href="@{http://www.thymeleaf.org}">Thymeleaf</a>
```

2. 条件求值
```
<a th:href="@{/login}" th:unless=${session.user != null}>Login</a>
```

3. for循环
```
<tr th:each="prod : ${prods}">
      <td th:text="${prod.name}">Onions</td>
      <td th:text="${prod.price}">2.41</td>
      <td th:text="${prod.inStock}? #{true} : #{false}">yes</td>
</tr>
```

#### Gradle 构建工具

spring 项目建议使用Gradle进行构建项目，相比maven来讲 Gradle更简洁，而且gradle更时候大型复杂项目的构建。
gradle吸收了maven和ant的特点而来，不过目前maven仍然是Java界的主流。

#### WebJars

1. 什么是WebJars?

WebJars是将客户端（浏览器）资源（JavaScript，Css等）打成jar包文件，以对资源进行统一依赖管理。WebJars的jar包部署在Maven中央仓库上。

2. 为什么使用

我们在开发Java web项目的时候会使用像Maven，Gradle等构建工具以实现对jar包版本依赖管理，以及项目的自动化管理，但是对于JavaScript，Css等前端资源包，我们只能采用拷贝到webapp下的方式，这样做就无法对这些资源进行依赖管理。那么WebJars就提供给我们这些前端资源的jar包形势，我们就可以进行依赖管理。

3. 如何使用

[WebJars主官网](http://www.webjars.org/) 查找对于的组件,在页面引入
