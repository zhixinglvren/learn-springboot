package com.learn.springboot;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WebPropertiesTests {

    @Autowired
    private WebProperties webProperties;

    @Test
    public void getHello() throws Exception {
        System.out.println(webProperties.getTitle());
        Assert.assertEquals(webProperties.getTitle(),"Spring Boot");
    }
}
