package com.learn.springboot.domain;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA
 */
public interface UserRepository extends JpaRepository<User,Long> {

    User findByUserName(String userName);

    User findByUserNameOrEmail(String userName, String email);
}
