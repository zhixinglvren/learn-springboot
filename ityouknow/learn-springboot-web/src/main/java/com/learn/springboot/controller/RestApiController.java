package com.learn.springboot.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * json 接口开发
 */
@RestController
public class RestApiController {

    private Logger logger = LoggerFactory.getLogger(RestApiController.class);

    @RequestMapping("/getJson")
    public Map<String,Object> getJson() {
        Map<String,Object> jsonMap = new HashMap<String,Object>();
        jsonMap.put("Name","Spring Boot");
        jsonMap.put("Version","1.5.9");

        logger.info("jsonMap=" + jsonMap);

        return jsonMap;
    }
}