### 什么是Spring Boot
Spring Boot是由Pivotal团队提供的全新框架，其设计目的是用来简化新Spring应用的初始搭建以及开发过程。该框架使用了特定的方式来进行配置，从而使开发人员不再需要定义样板化的配置。

### 平常搭建Spring Web项目时需要做什么？
* 配置web.xml，加载spring和spring mvc
* 配置数据库连接、配置spring事务
* 配置加载配置文件的读取，开启注解
* 配置日志文件
* ...
* 配置完成之后部署tomcat 调试
* ...

### 快速入门
访问[http://start.spring.io/](http://start.spring.io/)快速创建Maven工程

Spring Boot的基础结构共三个文件
* src/main/java 程序开发以及主程序入口
* src/main/resources 配置文件
* src/test/java 测试程序

Spring Boot建议的目录结果如下：
```
com
  +- example
    +- myproject
      +- Application.java
      |
      +- domain
      |  +- Customer.java
      |  +- CustomerRepository.java
      |
      +- service
      |  +- CustomerService.java
      |
      +- controller
      |  +- CustomerController.java
      |
```
* Application.java 建议放到根目录下面,主要用于做一些框架配置
* domain目录主要用于实体（Entity）与数据访问层（Repository）
* service 层主要是业务类代码
* controller 负责页面访问控制

### 总结
使用spring boot可以非常方便、快速搭建项目，使我们不用关心框架之间的兼容性，适用版本等各种问题，我们想使用任何东西，仅仅添加一个配置就可以，所以使用sping boot非常适合构建微服务。