package com.learn.springboot.sender;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

/**
 * Created by 知行旅人 on 2018/4/16.
 */
public class HelloSender {

    @Autowired
    private AmqpTemplate rabbitTemplate;

    public void send() {
        String context = "hello " + new Date();
        System.out.println("Sender :" + context);
        this.rabbitTemplate.convertAndSend("hello", context);
    }
}
