package com.learn.springboot;

import com.learn.springboot.sender.HelloSender;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by 知行旅人 on 2018/4/16.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class HelloSenderTests {

    @Autowired
    private HelloSender helloSender;

    @Test
    public void hello() throws Exception {
        helloSender.send();
    }
}
